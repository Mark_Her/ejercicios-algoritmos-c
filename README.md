# Ejercicios algoritmos C

En este repositorio voy a subir mi progreso resolviendo los ejercicios del libro [_"Programación estructurada a fondo, implementación de algoritmos en C, Sznajdleder Pablo"_](https://alfaomegaeditor.com.ar/producto/libro-programacion-estructurada-a-fondo-implementacion-de-algoritmos-en-c/).

## Contenido

1. [Introducción a los algoritmos y a la programación de computadoras](https://github.com/6d61726b/ejercicios-algoritmos-c/tree/main/capitulo-1)

2. [Estructuras básicas de control y lógica algorítmica](https://github.com/6d61726b/ejercicios-algoritmos-c/tree/main/capitulo-2)

3. [Funciones, modularización y metodología top-down](https://github.com/6d61726b/ejercicios-algoritmos-c/tree/main/capitulo-3)

4. [Tipos de datos alfanuméricos](https://github.com/6d61726b/ejercicios-algoritmos-c/tree/main/capitulo-4)

5. [Punteros a carácter](https://github.com/6d61726b/ejercicios-algoritmos-c/tree/main/capitulo-5)

6. [Punteros, arrays y aritmética de direcciones](https://github.com/6d61726b/ejercicios-algoritmos-c/tree/main/capitulo-6)

7. [Tipos de datos estructurados](https://github.com/6d61726b/ejercicios-algoritmos-c/tree/main/capitulo-7)

8. [Operaciones sobre archivos](https://github.com/6d61726b/ejercicios-algoritmos-c/tree/main/capitulo-8)

9. [Tipo Abstracto de Dato (TAD)](https://github.com/6d61726b/ejercicios-algoritmos-c/tree/main/capitulo-9)

10. Análisis de ejercicios integradores

11. Estructuras de datos dinámicas lineales